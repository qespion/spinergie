const fs = require('fs')

const entryJson = JSON.parse(fs.readFileSync('./test.json', 'utf8'))
const input = fs.readFileSync('./input.txt', 'utf8')

const set = (path, value) => {
  let current = entryJson
  const pathKeys = path.replace(']', '').split(/[\[.]/)
  for (let i = 0; i < pathKeys.length; ++i) {
    const key = /^[0-9]+$/.test(pathKeys[i])
      ? parseInt(pathKeys[i])
      : pathKeys[i]
    if (i + 1 === pathKeys.length) {
      current[key] = value
    } else if (current[key]) {
      current = current[key]
    } else {
      current[key] = /^[0-9]+$/.test(pathKeys[i + 1]) ? [] : {}
      current = current[key]
    }
  }
}

const run = () => {
  const lines = input.split('\n')
  lines.forEach((line) => {
    const [path, ...value] = line.replace(' ', '').split(':')
    set(path.replace(/"/g, ''), JSON.parse(value.join(':')))
  })
  fs.writeFileSync('./output.json', JSON.stringify(entryJson, null, 2))
}

run()
